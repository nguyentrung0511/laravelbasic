<?php
namespace App\Repositories\Post;

use App\Repositories\BaseRepository;

class PostRepository extends BaseRepository implements PostRepositoryInterface
{
    /**
     * Get Model Post
     */
    public function getModel()
    {
        return \App\Models\Post::class;
    }
}